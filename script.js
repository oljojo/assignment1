
const work = {
    //work bank button
    workBankButton: document.getElementById("bankWorkB"),
    //work button
    workButton: document.getElementById("workB"),
    //text of the pay balance
    payBalanceAmount: document.getElementById("payBalance"),
    //creating pending loan field
    pendingLoan: document.createElement("div"),
    //creating pending loan text
    pendingLoanText: document.createElement("span"),
    //creating pending loan amount
    pendingLoanAmount: document.createElement("span"),
    //creating repay loan button
    loanPayback: document.createElement("input"),
    updateLoanInfo() {
        this.pendingLoanText.textContent = "Pending Loan:"
        this.loanPayback.type = "button"
        this.loanPayback.value = "Repay loan"
    },
    //update the pay balance
    updatePB() {
        
        this.payBalanceAmount.textContent = new Intl.NumberFormat('se-SE', {style: 'currency', currency: 'SEK'}).format(projVariables.payBalance);
    },
    //update pending loan
    updatePL () {
        this.pendingLoan.classList.add("row")
        this.loanPayback.style = "width: 70%; margin: auto;"
        this.pendingLoanAmount.textContent = new Intl.NumberFormat('se-SE', {style: 'currency', currency: 'SEK'}).format(projVariables.loan);
    },
    //remove the loan from the document
    noLoan() {
        this.pendingLoan.remove()
        this.pendingLoanText.remove()
        this.pendingLoanAmount.remove()
        this.loanPayback.remove()
        projVariables.numLoans --
    }
    
}

const bank = {
    //text of the bank balance
    bankBalanceAmount: document.getElementById("bankBalance"),
    //get loan button
    getLoanButton: document.getElementById("getLoanB"),
    //update the bank balance
    updateBB() {
        this.bankBalanceAmount.textContent = new Intl.NumberFormat('se-SE', {style: 'currency', currency: 'SEK'}).format(projVariables.bankBalance);
    },
}

const projVariables = {
    //initial bank balance
    bankBalance: 0,
    //initial pay balance
    payBalance: 0,
    //number of loans
    numLoans: 0,
    //initilizing loan amount
    loan: 0,
}

const clickFunctions = {
    //function for clicking work button
    workClick(){
        projVariables.payBalance += 100
    
        work.updatePB()
    },
    //function for clicking the bank button
    bankClick(){
        if(projVariables.numLoans < 1){
            projVariables.bankBalance += projVariables.payBalance
        }
        else{
            if(projVariables.payBalance * 0.1 < projVariables.loan){
                projVariables.bankBalance += projVariables.payBalance * 0.9
                projVariables.loan -= projVariables.payBalance * 0.1
            }
            else{
                projVariables.bankBalance += (projVariables.payBalance * 0.9) + (projVariables.payBalance * 0.1) - projVariables.loan
                work.noLoan()
            }
        }
        
        projVariables.payBalance = 0
        work.updatePB()
        bank.updateBB()
        work.updatePL()
    },
    //function for clicking get loan button
    getLoanClick(){
        if(projVariables.numLoans < 1){
            projVariables.loan = Number(prompt(`Enter loan amount(max ${projVariables.bankBalance * 2}"): `, projVariables.bankBalance * 2))
            if(projVariables.loan > 0){
                projVariables.bankBalance += projVariables.loan
                bank.updateBB()
                projVariables.numLoans ++
                work.updatePL()
                work.updateLoanInfo()
                work.payBalanceAmount.after(work.pendingLoan)
                work.pendingLoan.appendChild(work.pendingLoanText)
                work.pendingLoan.appendChild(work.pendingLoanAmount)
                work.pendingLoan.appendChild(work.loanPayback)
            }
        }
        else{
            alert("Maximum number of loans(one) already accuired!")
        }
        work.updatePB()
        bank.updateBB()
        work.updatePL()
        
    },
    //function for clicking the repay loan button
    repayLoanClick(){
        if(projVariables.loan <= projVariables.payBalance){
            projVariables.payBalance = projVariables.payBalance - projVariables.loan
            projVariables.loan = 0
            work.noLoan()
        }
        else{
            projVariables.loan -= projVariables.payBalance
            projVariables.payBalance = 0
        }
        bank.updateBB()
        work.updatePB()
        work.updatePL()
    },
}








//click function to get a loan
bank.getLoanButton.addEventListener("click", clickFunctions.getLoanClick)

//click function to bank the worked pay
work.workBankButton.addEventListener("click", clickFunctions.bankClick)

//click function to "work"
work.workButton.addEventListener("click", clickFunctions.workClick)

//click function for the button to pay back the loan
work.loanPayback.addEventListener("click", clickFunctions.repayLoanClick)



const laptopVars = {
    laptopList: document.getElementById("laptopList"),
    laptopInfo: document.getElementById("laptopInfo"),
    image: document.createElement("img"),
    name: document.createElement("span"),
    description: document.createElement("span"),
    price: document.createElement("span"),
    buyNowButton: document.createElement("input"),
    textDiv: document.createElement("div"),
    laptopFeatures: document.getElementById("features")
}

laptopVars.laptopList.addEventListener("change", updateLL)

laptopVars.buyNowButton.addEventListener("click", buyNow)

function buyNow() {
    if (apiLaptops[Number(laptopVars.laptopList.value) - 1].price <= projVariables.bankBalance){
        //buy
        projVariables.bankBalance = projVariables.bankBalance - Number(apiLaptops[Number(laptopVars.laptopList.value) - 1].price)
        alert(`You are now the owner of ${apiLaptops[Number(laptopVars.laptopList.value) - 1].title} `)
    }
    else{
        //error, not enough money
        alert("Not enough money!")
    }
    bank.updateBB()
}

function updateLL() {

    laptopVars.image.src = "https://noroff-komputer-store-api.herokuapp.com/" + apiLaptops[Number(laptopVars.laptopList.value) - 1].image
    laptopVars.image.classList.add("img-thumbnail")
    laptopVars.image.classList.add("img-fluid")
    laptopVars.image.classList.add("col")
    laptopVars.image.style = "max-width: 30%;"
    laptopVars.textDiv.classList.add("col")
    laptopVars.textDiv.style = "margin: 20px;"
    laptopVars.name.classList.add("row")
    laptopVars.name.textContent = apiLaptops[Number(laptopVars.laptopList.value) - 1].title
    laptopVars.description.classList.add("row")
    laptopVars.description.textContent = apiLaptops[Number(laptopVars.laptopList.value) - 1].description
    laptopVars.price.classList.add("row")
    laptopVars.price.textContent = "Price: " + apiLaptops[Number(laptopVars.laptopList.value) - 1].price
    laptopVars.buyNowButton.style = "margin: 50px; float: right;"
    laptopVars.buyNowButton.classList.add("row")
    laptopVars.buyNowButton.type = "button"
    laptopVars.buyNowButton.value = "Buy Now"
    let specsArr = new Array
    apiLaptops[Number(laptopVars.laptopList.value) - 1].specs.forEach(element => {
        specsArr.push(element)
    })
    laptopVars.laptopFeatures.textContent = specsArr.join(". ")

    laptopVars.laptopInfo.appendChild(laptopVars.image)
    laptopVars.laptopInfo.appendChild(laptopVars.textDiv)
    laptopVars.textDiv.appendChild(laptopVars.name)
    laptopVars.textDiv.appendChild(laptopVars.description)
    laptopVars.textDiv.appendChild(laptopVars.price)
    laptopVars.textDiv.appendChild(laptopVars.buyNowButton)


}

function fillLaptopList (laptops){
    laptops.forEach((laptop) => {
        const laptopOption = document.createElement("option")
        laptopOption.text = laptop.title
        laptopOption.value = laptop.id
        laptopVars.laptopList.appendChild(laptopOption)


        laptopVars.image.src = "https://noroff-komputer-store-api.herokuapp.com/" + apiLaptops[Number(laptopVars.laptopList.value) - 1].image
        laptopVars.image.classList.add("img-thumbnail")
        laptopVars.image.classList.add("img-fluid")
        laptopVars.image.classList.add("col")
        laptopVars.image.style = "max-width: 30%;"
        laptopVars.textDiv.classList.add("col")
        laptopVars.textDiv.style = "margin: 20px;"
        laptopVars.name.classList.add("row")
        laptopVars.name.textContent = apiLaptops[Number(laptopVars.laptopList.value) - 1].title
        laptopVars.description.classList.add("row")
        laptopVars.description.textContent = apiLaptops[Number(laptopVars.laptopList.value) - 1].description
        laptopVars.price.classList.add("row")
        laptopVars.price.textContent = "Price: " + apiLaptops[Number(laptopVars.laptopList.value) - 1].price
        laptopVars.buyNowButton.style = "margin: 50px; float: right;"
        laptopVars.buyNowButton.classList.add("row")
        laptopVars.buyNowButton.type = "button"
        laptopVars.buyNowButton.value = "Buy Now"
        let specsArr = new Array
        apiLaptops[Number(laptopVars.laptopList.value) - 1].specs.forEach(element => {
            specsArr.push(element)
        })
        laptopVars.laptopFeatures.textContent = specsArr.join(". ")
    
        laptopVars.laptopInfo.appendChild(laptopVars.image)
        laptopVars.laptopInfo.appendChild(laptopVars.textDiv)
        laptopVars.textDiv.appendChild(laptopVars.name)
        laptopVars.textDiv.appendChild(laptopVars.description)
        laptopVars.textDiv.appendChild(laptopVars.price)
        laptopVars.textDiv.appendChild(laptopVars.buyNowButton)

    })
}

//api request function
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => apiLaptops = data)
    .then(apiLaptops => fillLaptopList(apiLaptops))

//initialize the pay balance and bank balance
window.addEventListener("load", () => {
    bank.updateBB()
    work.updatePB()
})